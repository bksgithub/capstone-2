const express = require("express")
const dotenv = require("dotenv")
const mongoose = require("mongoose")
const cors = require("cors")
const userRoutes = require("./routes/userRoutes")
const orderRoutes = require("./routes/orderRoutes")

dotenv.config()

const app = express()
const port = 8002

mongoose.connect(`mongodb+srv://bks0381:${process.env.MONGODB_PASSWORD}@cluster0.0atlgec.mongodb.net/capstone-2?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.once("open", () => console.log(`connected to mongodb`))

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())

//routes
app.use("/users", userRoutes)
app.use("/orders", orderRoutes)

app.listen(port, () => console.log(`API is now running on localhost: ${port}.`))