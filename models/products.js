const mongoose = require("mongoose")

const product_schema = new mongoose.Schema({

name: {
	type: String
},
description: {
	type: String
},
price: {
	type: Number
},
isActive: {
	type: Boolean,
	default: true
},
createdOn: {
	type: Date,
	default: new Date()
}
})

module.exports = mongoose.model("Products", product_schema)