const mongoose = require("mongoose")

const user_schema = new mongoose.Schema({

email: {
	type: String
},
password: {
	type: String
},
isAdmin: {
	type: Boolean,
	default: false
}
})

module.exports = mongoose.model("Users", user_schema)