const Order = require("../models/orders")
const User = require("../models/users")
const Product = require("../models/products")


module.exports.createOrder = async (data) => {

		if (data.isAdmin) {

			return Promise.resolve({
				message: "ADMIN is not allowed to check out."
			})
		} 

		else {
			let new_order = new Order({
			userId: data.userId,
			products: [{
				productId: data.productId, 
				quantity: data.quantity
			}],
			totalAmount: data.totalAmount
		})

			return new_order.save().then((result, error) => {
			if (error) {
				return false
			}

				return Promise.resolve({
			message: "Order successfully created."
		})
		})
		}

	}


module.exports.getOrders = () => {
	return Order.find({}).then((result, error) => {
		if (error) {
			return false
		}

		return result
	})
}