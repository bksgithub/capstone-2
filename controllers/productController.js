const Product = require("../models/products")

module.exports.retrieveAllProducts = () => {
	return Product.find({}).then((result) => {
		return result
	})
}

module.exports.retrieveSingleProduct = (product_id) => {
	return Product.findById(product_id).then((error, result) => {
		if (error) {
			return error
		}

		return result
	})
}

module.exports.updateProductInfo = (product_id, product_update) =>  {
	if (product_update.isAdmin) {
		return Product.findByIdAndUpdate(product_id, {
			name: product_update.product.name,
			description: product_update.product.description,
			price: product_update.product.price,
			isActive: product_update.product.isActive
		}).then((result, error) => {
			if (error) {
				return false
			}

			return {
				result: result,
				message: "Product has been updated successfully." 
			}
		})
	} 

	return Promise.resolve({
		message: "User must be ADMIN to update product information."
	})
	
}


module.exports.archive = (product_id,data) => {
	if (data.isAdmin) {
		return Product.findByIdAndUpdate(product_id, {isActive: data.product_status.isActive}).then((result, error) => {
		if (error) {
			return error
		}

		return {
			message: "Product has been archived."
		}
	})
	}

	return Promise.resolve({
		message: "User must be ADMIN to archive products."
	})
	
}