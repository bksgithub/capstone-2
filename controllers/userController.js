const User = require("../models/users")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const Product = require("../models/products")

module.exports.register = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		email: data.email,
		password: encrypted_password
})

	return new_user.save().then((new_user, error) => {
		if (error) {
			return error
		}

		return {
			message: "User successfully registered."
			}
		}
	)}


module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if (result == null) {
			return {
				message: "Incorrect username or password."
			}
		}

const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if (is_password_correct || result.password === data.password) {
			
			return {
				access: auth.createAccessToken(result),
				message: "You're logged in successfully."
			}
		}

			return {
				message: "Incorrect password."
			}
		

	})
}


module.exports.createProduct = (data) => {
	if (data.isAdmin) {

		let new_product = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return new_product.save().then((new_product, error) => {
			if (error) {
				return false
			}
			let message = {
				message: "Product has been added."
			}
			return message
		})
	}

	return Promise.resolve(
		{
		message: "User must be ADMIN to create product."
	})
	
}


module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id).then((result) => {
		return result
	})
}