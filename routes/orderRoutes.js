const express = require("express")
const router = express.Router()
const UserController = require("../controllers/userController")
const ProductController = require("../controllers/productController")
const authorization = require("../auth")
const OrderController = require("../controllers/orderController")

router.post("/create-order", authorization.verify, (request, response) => {
	const data = {
		isAdmin: authorization.decode(request.headers.authorization).isAdmin,
		userId: request.body.userId,
		productId: request.body.productId, 
		quantity: request.body.quantity,
		totalAmount: request.body.totalAmount
	}

	OrderController.createOrder(data).then((result) => {
		response.send(result)
	})
})

router.get("/retrieve-orders", (request, response) => {

	OrderController.getOrders().then((result) => {
		response.send(result)
	})
})

module.exports = router