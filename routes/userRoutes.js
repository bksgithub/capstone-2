const express = require("express")
const router = express.Router()
const UserController = require("../controllers/userController")
const ProductController = require("../controllers/productController")
const authorization = require("../auth")
const OrderController = require("../controllers/orderController")

//user registration
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

router.post("/create-product", authorization.verify, (request, response) => {

	const data = {
		product: request.body,
		isAdmin: authorization.decode(request.headers.authorization).isAdmin
	}
	UserController.createProduct(data).then((result) => {
		response.send(result)
	})
})


router.get("/retrieve-all-products", (request, response) => {
	ProductController.retrieveAllProducts().then((result) => {
		response.send(result)
	})
})


router.get("/:productId", (request, response) => {
	ProductController.retrieveSingleProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

router.patch("/:productId/update-product-info", authorization.verify, (request, response) => {

	const data = {
		product: request.body,
		isAdmin: authorization.decode(request.headers.authorization).isAdmin
	}

	ProductController.updateProductInfo(request.params.productId, data).then((result) => {
		response.send(result)
	})
})


router.patch("/:id/archive", authorization.verify, (request, response) => {

	const data = {
		product_status: request.body,
		isAdmin: authorization.decode(request.headers.authorization).isAdmin
	}
	ProductController.archive(request.params.id, data).then((result) => {
		response.send(result)
	})
})

router.get("/:id/retrieve-user-details", (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})


module.exports = router